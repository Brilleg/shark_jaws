﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLives : MonoBehaviour {

    public int lives = 0;
    public static PlayerLives instanciate;

    void Awake()
    {
        if (PlayerLives.instanciate == null)
        {
            PlayerLives.instanciate = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    public void ReduceLives()
    {
        lives = lives - 1;
    }

    public int ReturnLives()
    {
        return lives;
    }
}
