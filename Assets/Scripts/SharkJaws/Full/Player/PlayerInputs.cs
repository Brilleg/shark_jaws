﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : MonoBehaviour {

    public float speed = 0;
    public static PlayerInputs instanciate;
    private Vector3 positionLimit;

    void Update()
    {
        Movement_player();
    }

    void Awake()
    {
        if (PlayerInputs.instanciate == null)
        {
            PlayerInputs.instanciate = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    //----------Control del movimiento del jugador----------//

    void Movement_player()
    {
        transform.Translate(speed * Input.GetAxis("Horizontal") * Time.deltaTime, speed * Input.GetAxis("Vertical") * Time.deltaTime, 0f);

        positionLimit = Camera.main.WorldToViewportPoint(transform.position);
        positionLimit.x = Mathf.Clamp01(positionLimit.x);
        positionLimit.y = Mathf.Clamp01(positionLimit.y);
        transform.position = Camera.main.ViewportToWorldPoint(positionLimit);
    }
}
