﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEngine : MonoBehaviour {

    private SpriteRenderer playerSprite;

    private void Start()
    {
        //Captura la referncia del SpriteRender para este gameobject
        playerSprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Flip_sprite();
    }

    //------------Controlador del "Flip" para el Sprite---------------//
    void Flip_sprite()
    {
        if (playerSprite != null)
        {
            //Si la flecha izquierda es precionada voltea el sprite en x
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                //Voltea a la izquierda el sprite
                playerSprite.flipX = true;
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                //Voltea a la derecha el sprite
                playerSprite.flipX = false;

            }
        }
    }
}
