﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {

    public static GameControl instance;


    void Start () {
		
	}
	
	void Update () {
		
	}

    // Use this for initialization
    void Awake()
    {
        if (GameControl.instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void LoadScene(string value)
    {
        SceneManager.LoadScene(value);
    }
}
