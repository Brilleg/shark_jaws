﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour {

    public GameObject creditsPanel;
    public GameObject pausePanel;
    public bool isPauseActive = false;

    void Start () {
		
	}
	
	void Update () {
        if (pausePanel != null && Input.GetKeyDown(KeyCode.Space))
        {
            if (isPauseActive == false)
            {
                StopGame();
            }
            else if(isPauseActive == true)
            {
                ContinueGame();
            }
        }
    }


    public void QuitGame()
    {
        Application.Quit();
    }

    public void NextScene(string sceneName)
    {
        ContinueGame();
        if (GameControl.instance != null)
        {
            GameControl.instance.LoadScene(sceneName);
        }
        else
        {
            Debug.LogError("GameControl without instance");
        }
    }

    public void StopGame()
    {
        if (pausePanel != null)
        {
            pausePanel.SetActive(true);
            isPauseActive = true;
        }
        Time.timeScale = 0f;
    }

    public void ContinueGame()
    {
        if (pausePanel != null)
        {
            pausePanel.SetActive(false);
            isPauseActive = false;
        }
        Time.timeScale = 1f;
    }

    public void SeeCredits()
    {
        if (creditsPanel != null)
        {
            creditsPanel.SetActive(true);

        }
        else
        {
            Debug.LogError("Verificar si el panel de créditos se encuntra en la escena");
        }
    }

    public void HideCredits()
    {
        if (creditsPanel != null)
        {
            creditsPanel.SetActive(false);

        }
        else
        {
            Debug.LogError("Verificar si el panel de créditos se encuntra en la escena");
        }
    }
}
