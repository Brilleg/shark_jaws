﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fish : MonoBehaviour {

    public float speed;
    public Transform moveSpot;
    private SpriteRenderer targetSprite;
    public float minX;
    public float minY;
    public float maxX;
    public float maxY;
    //private int score = 0;
    //private Text score_text;


    void Update () {

        Movement_target();
        FishPatrol();

    }

    //Se llama cuando el juego inicia
    private void Awake()
    {
        //Captura la referncia del SpriteRender para este gameobject
        targetSprite = GetComponent<SpriteRenderer>();
    }

    void Movement_target()
    {
        Physics2D.IgnoreLayerCollision(8, 9);

        //Movimentos random por la pantalla
        transform.position = Vector2.MoveTowards(transform.position, moveSpot.position, speed * Time.deltaTime);


        //Si la variable no es nula se puede hacer la referencia al sprite
        if (targetSprite != null)
        {
            //Si la flecha izquierda es precionada voltea el sprrite en x
            if (moveSpot.position.x > transform.position.x)
            {
                //Voltea a la izquierda el sprite
                targetSprite.flipX = true;
            }

            if (moveSpot.position.x < transform.position.x)
            {

                //Voltea a la derecha el sprite
                targetSprite.flipX = false;

            }
        }
    }

    void FishPatrol()
    {
        //Random para la patrulla del pez en pantalla
        if (Vector2.Distance(transform.position, moveSpot.position) < 0.2f)
        {
            moveSpot.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        }
    }


}
