﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shark : MonoBehaviour {

    public float findSpeed;
    public float patrolSpeed;
    private Transform target;
    public Transform patrolSpot;
    public float minX;
    public float minY;
    public float maxX;
    public float maxY;

    void Start () {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
	
    void Update()
    {
        Movement_enemy();
        Patrol_enemy();
        Wrap_screen();
    }

//------------Control de movimento del enemigo--------------//

    void Movement_enemy() {
        //Mide la distancia entre el jugador y el enemigo; definiendo así la acción de seguir o patrullar
        if (target != null)
        {
            //Inicia la persecución
            if (Vector2.Distance(transform.position, target.position) < 5)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, findSpeed * Time.deltaTime);

                if (target.position.x > transform.position.x)
                {
                    //Voltear a la Derecha
                    transform.localScale = new Vector2(1, 1);
                }
                else if (target.position.x < transform.position.x)
                {
                    //Voltear a la Izquierda
                    transform.localScale = new Vector2(-1, 1);
                }
            }
            else
            {
                //Inicia el movimiento de la patrulla
                transform.position = Vector2.MoveTowards(transform.position, patrolSpot.position, patrolSpeed * Time.deltaTime);
                if (patrolSpot.position.x > transform.position.x)
                {
                    //Voltear a la Derecha
                    transform.localScale = new Vector2(1, 1);
                }
                else if (patrolSpot.position.x < transform.position.x)
                {
                    //Voltear a la Izquierda
                    transform.localScale = new Vector2(-1, 1);
                }
            }
        }
    }

    //El metodo genera puntos aleatorios y asi crear el recorrido de la patrulla

    void Patrol_enemy(){

        //Random para la patrulla del tiburón en pantalla
        if (Vector2.Distance(transform.position, patrolSpot.position) < 0.2f)
        {
            patrolSpot.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        }

    }

//---------------Control del "Wrap" del enemigo en pantalla--------------------//

    void Wrap_screen(){
        //Wrap de derecha a izquierda
        if (target != null)
        {
            if (target.position.x <= -7 && transform.position.x > 5)
            {
                //Voltear a la Derecha
                transform.localScale = new Vector2(1, 1);

                transform.position = Vector2.MoveTowards(transform.position, (transform.position) * 2, findSpeed * Time.deltaTime);
                if (transform.position.x > 8)
                {
                    if (target.position.y > 0)
                    {
                        transform.position = new Vector2(-9, Random.Range(0, 5));
                    }
                    if (target.position.y < 0)
                    {
                        transform.position = new Vector2(-9, Random.Range(0, -5));
                    }
                }
            }
        }//Fin del wrap derecho


        //Wrap de izquierda a derecha
        if (target != null)
        {
            if (target.position.x >= 7 && transform.position.x < -5)
            {
                //Voltear a la Izquierda
                transform.localScale = new Vector2(-1, 1);
                transform.position = Vector2.MoveTowards(transform.position, (transform.position) * 2, findSpeed * Time.deltaTime);
                if (transform.position.x < -8)
                {
                    if (target.position.y > 0)
                    {
                        transform.position = new Vector2(9, Random.Range(0, 5));
                    }
                    if (target.position.y < 0)
                    {
                        transform.position = new Vector2(9, Random.Range(0, -5));
                    }
                }
            }
        }//Fin del wrap izquierdo
    }//Fin del metodo Wrap_screen    

}

