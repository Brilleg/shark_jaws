﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colisiones : MonoBehaviour {

    //--------Control de colisones con el enemigo---------//----Las coliciones van a parte

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Lives.instanciate.disminuirVidas();
            Physics2D.IgnoreLayerCollision(9, 10);
            StartCoroutine(InmortalTime(1.5F));
            Debug.Log(Lives.instanciate.retornarVidas());

            if (Lives.instanciate.retornarVidas() == 0)
            {
            Destroy(gameObject);
            }
        }

        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }

    IEnumerator InmortalTime(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Physics2D.IgnoreLayerCollision(9, 10, false);
    }
}
