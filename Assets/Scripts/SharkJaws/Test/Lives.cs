﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lives : MonoBehaviour {

    public int lives = 0;
    public static Lives instanciate;



    void Update () {
    }

    void Awake()
    {
        if (Lives.instanciate == null)
        {
            Lives.instanciate = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    public void disminuirVidas()
    {
        lives = lives - 1;
    }

    public int retornarVidas()
    {
        return lives;
    }
}
